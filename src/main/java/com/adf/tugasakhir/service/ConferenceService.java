package com.adf.tugasakhir.service;

import java.util.List;

import javax.persistence.EntityNotFoundException;

import com.adf.tugasakhir.model.Conference;

/**
 * ConferenceService
 */
public interface ConferenceService {

    /**
     * Get all of the latest conference. Will sort the conference by the date it's
     * happening, not by id.
     * 
     * @return
     */
    List<Conference> getAllLatestConference();

    /**
     * Will take a conference and then save it to the database. The programmer
     * shouldn't set the conference id by themselve, i's updated automatically.
     * 
     * @param conference
     */
    void addConference(Conference conference);

    /**
     * Will remove the conference from the database. Even if the id doesn't exist
     * anymore, it won't throw an exception because it's idempotent.
     * 
     * @param id
     */
    void removeConferenceById(Long id);

    /**
     * Get the conference by their id. The programmer must check whether it's found
     * or not by implementing a catch on the throwable.
     * 
     * @param id of the conference
     * @return a conference, ready to use.
     * @throws EntityNotFoundException when the entity doesn't exist in the
     *                                 database.
     */
    Conference getConferenceById(Long id);

    /**
     * Update the conference.
     * 
     * @param conference must provide an id that's inside of the database.
     * @throws EntityNotFoundException if the id doesn't exist in the database.
     */
    void updateConference(Conference conference);
}