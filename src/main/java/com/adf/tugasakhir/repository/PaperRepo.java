package com.adf.tugasakhir.repository;

import com.adf.tugasakhir.dataclass.Paper;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaperRepo extends JpaRepository<Paper, Long> {
}
