package com.adf.tugasakhir.dataclass;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Paper {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;
    private String abstrak;
    private String url;

    public Paper(String title, String url, String abstrak) {
        this.title = title;
        this.abstrak = abstrak;
        this.url = url;
    }

    public Paper(){}

    public String getTitle() {
        return title;
    }

    public String getAbstrak() {
        return abstrak;
    }

    public String getUrl() {
        return url;
    }

    @Override
    public String toString() {
        return "Paper{" + "title='" + title + '\'' + ", abstrak='" + abstrak + '\'' + ", url='" + url + '\'' + '}';
    }
}
