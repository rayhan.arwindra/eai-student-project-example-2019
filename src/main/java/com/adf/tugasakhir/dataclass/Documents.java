package com.adf.tugasakhir.dataclass;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Documents
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Documents {

    @JsonProperty("documents")
    List<Document> documentList = new ArrayList<>();

    public Documents() {
    }

    public Documents(List<Document> documents) {
        this.documentList = documents;
    }

    public List<Document> getDocuments() {
        return this.documentList;
    }

    public void setDocuments(List<Document> documents) {
        this.documentList = documents;
    }

    public void addDocument(Document document) {
        document.setId(String.valueOf(documentList.size() + 1));
        documentList.add(document);
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Documents)) {
            return false;
        }
        Documents documentsFound = (Documents) o;
        return Objects.equals(documentsFound, documentsFound);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(documentList);
    }

    @Override
    public String toString() {
        return "{" +
            " documents='" + getDocuments() + "'" +
            "}";
    }
}