package com.adf.tugasakhir.controller;


import javax.persistence.EntityNotFoundException;

import com.adf.tugasakhir.dataclass.Paper;
import com.adf.tugasakhir.dataclass.PaperResult;
import com.adf.tugasakhir.model.Conference;
import com.adf.tugasakhir.repository.ConferenceRepo;
import com.adf.tugasakhir.repository.PaperRepo;
import com.adf.tugasakhir.service.ConferenceService;
import com.adf.tugasakhir.service.PaperFinderService;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Log4j2
@Controller
public class HomeController {

    @Autowired
    PaperFinderService paperFinderService;

    @Autowired
    ConferenceService conferenceService;

    @Autowired
    PaperRepo paperRepo;

    @Autowired
    ConferenceRepo conferenceRepo;

    private static final String CONFERENCE = "conference";
    private static final String ERROR_MSG = "error_msg";
    private static final String REDIRECT_GOOF = "redirect:/goof";
    private static final String REDIRECT = "redirect:/";

    @GetMapping("/")
    @PreAuthorize("isAuthenticated()")
    public String home(Model model) {
        model.addAttribute("conferenceList", conferenceService.getAllLatestConference());
        return "_home";
    }

    @GetMapping("/add-conference")
    @PreAuthorize("isAuthenticated()")
    public String addConferenceForm(Model model) {
        model.addAttribute(CONFERENCE, new Conference());
        return "add-conference";
    }

    @PostMapping("/add-conference")
    @PreAuthorize("isAuthenticated()")
    public String addConferenceSubmit(@ModelAttribute Conference newConference, BindingResult bindingResult,
            RedirectAttributes redAttr) {
        if (bindingResult.hasErrors()) {
            redAttr.addFlashAttribute(ERROR_MSG, "Terdapat kesalahan pada form ada.");
            log.error("Something went wrong with the form");
            return REDIRECT_GOOF;
        }
        conferenceRepo.save(newConference);
        log.info("Added new conference");
        return REDIRECT;
    }
    @GetMapping("/add-paper")
    @PreAuthorize("isAuthenticated()")
    public String addPaper(Model model) {
        model.addAttribute("paper", new Paper());
        return "add-paper";
    }
    @PostMapping("/add-paper")
    @PreAuthorize("isAuthenticated()")
    public String addPaperSubmit(@ModelAttribute Paper newPaper, BindingResult bindingResult,
                                      RedirectAttributes redAttr) {
        if (bindingResult.hasErrors()) {
            redAttr.addFlashAttribute(ERROR_MSG, "Terdapat kesalahan pada form ada.");
            log.error("Something went wrong with the form");
            return REDIRECT_GOOF;
        }
        paperRepo.save(newPaper);
        log.info("Added new paper");
        return REDIRECT;
    }

    @GetMapping("/update/{id}")
    @PreAuthorize("isAuthenticated()")
    public String updateConferenceForm(@PathVariable("id") long id, RedirectAttributes redirectAttributes,
            Model model) {

        try {
            model.addAttribute(CONFERENCE, conferenceService.getConferenceById(id));
        } catch (EntityNotFoundException e) {
            redirectAttributes.addFlashAttribute(ERROR_MSG, "Conference not found.");
            log.error("Conference not found");
            return REDIRECT_GOOF;
        }
        return "update-conference";
    }

    @PostMapping("/update-conference")
    @PreAuthorize("isAuthenticated()")
    public String updateConferenceSubmit(@ModelAttribute Conference conference, RedirectAttributes redirectAttributes) {
        try {
            conferenceService.updateConference(conference);
        } catch (EntityNotFoundException e) {
            redirectAttributes.addFlashAttribute(ERROR_MSG, "The conference that you want to update doesn't exist.");
            log.error("Something went wrogn updating conference");
            return REDIRECT_GOOF;
        }
        log.info("Succesfully updated conference");
        return REDIRECT;
    }

    @PostMapping("/delete/{id}")
    @PreAuthorize("isAuthenticated()")
    public String deleteConference(@PathVariable("id") long id) {
        conferenceService.removeConferenceById(id);
        log.info("Deleted conference");
        return REDIRECT;
    }

    @GetMapping("/view/{id}")
    @PreAuthorize("isAuthenticated()")
    public String viewConference(@PathVariable("id") long id, Model model, RedirectAttributes redAttr) {
        try {
            Conference conference = conferenceService.getConferenceById(id);
            model.addAttribute(CONFERENCE, conference);
            PaperResult pResult = paperFinderService.findPaperFromConference(conference);

            if (!pResult.isError()) {
                model.addAttribute("paperList", pResult.getPaperList());
            }
            return "conference-detail";
        } catch (EntityNotFoundException e) {
            redAttr.addFlashAttribute(ERROR_MSG, "Conference not found.");
            return REDIRECT_GOOF;
        }
    }

    @GetMapping("/goof")
    @PreAuthorize("isAuthenticated()")
    public String customErrorPage(Model model) {
        return "error/custom_error";
    }
}