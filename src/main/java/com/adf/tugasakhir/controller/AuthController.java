package com.adf.tugasakhir.controller;

import java.security.Principal;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@Slf4j
public class AuthController {

  @GetMapping("/login")
  @PreAuthorize("permitAll()")
  public String login(final Principal principal) {
    if (principal != null) {
      return "redirect:/";
    }
    return "_login.html";
  }
}
